import { Component, OnInit } from '@angular/core';

import { ShoppingCartService } from "./shopping-cart.service";

@Component({
  selector: 'mt-shopping-cart',
  templateUrl: './shopping-cart.component.html'
})
export class ShoppingCartComponent implements OnInit {

  constructor(private shoppingCardService: ShoppingCartService) { }

  ngOnInit() {
  }

  items(): any[] {
    return this.shoppingCardService.items
  }

  total(): number {
    return this.shoppingCardService.total()
  }

  clear() {
    this.shoppingCardService.clear()
  }

  removeItem(item: any) {
    this.shoppingCardService.removeItem(item)
  }

  addItem(item: any) {
    this.shoppingCardService.addItem(item);
  }

}
